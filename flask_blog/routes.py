from flask_blog.models import User, Post
from flask_blog.forms import RegistrationForm, LoginForm, UpdateAccountForm, PostForm
from flask_blog import app, db, bcrypt
from flask import render_template, redirect, url_for, flash, request, abort
from flask_login import login_user, current_user, logout_user, login_required
#import secrets
import os
from os import urandom
from PIL import Image


@app.route("/")
@app.route("/home")
def home():
    title = "Home Page"
    posts = Post.query.all()
    return render_template("index.html", posts= posts, title=title)

@app.route("/about")
def about():
    title = "About Us"
    return render_template("about.html", title=title)

@app.route("/register", methods=["GET", "POST"])
def register():
    title = "Create Account"
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_pwd = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_pwd)
        db.session.add(user)
        db.session.commit()
        flash("Your account has been created successfully! You are now able to log in.", "success")
        flash("OK!", "success")
        return redirect(url_for("login"))
    
    return render_template("register.html", title=title, form=form)

@app.route("/login", methods=["GET", "POST"])
def login():
    title = "Login"
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            flash("Logged in Successfully!", "success")
            return redirect(next_page if next_page else url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template("login.html", title=title, form=form)

@app.route("/logout", methods=["GET", "POST"])
def logout():
    title = "Logout"
    logout_user()
    return redirect(url_for("home"))

def save_picture(form_picture):
    #random_hex = secrets.token_hex(8)
    random_hex = urandom(8).hex()
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)
    output_size = (125,125)
    img = Image.open(form_picture)
    img.thumbnail(size=output_size)
    img.save(picture_path)
    return picture_fn

@app.route("/account", methods=["GET", "POST"])
@login_required
def account():
    title = "Account"
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash("Your account has been updated!", 'success')
        return redirect(url_for('account'))
    elif request.method == "GET":
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template("account.html", img_file = image_file, title=title, form=form)

@app.route("/post/new", methods=["GET", "POST"])
@login_required
def new_post():
    title = "Create New Post"
    form = PostForm()
    if form.validate_on_submit():
        #if form.title.data and form.content.data:
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash("Your post has been created!", "success")
        return redirect(url_for('home'))
    return render_template("create_post.html", form=form, title=title)

@app.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template("post.html", title=post.title, post=post)

@app.route("/post/<int:post_id>/update")
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403, description="Forbidden")
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash("Your post has been update successfully!", 'success')
        return redirect(url_for('post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('create_post.html', title='Update Post',
                           form=form, legend='Update Post')

@app.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash('Your post has been deleted!', 'success')
    return redirect(url_for('home'))


